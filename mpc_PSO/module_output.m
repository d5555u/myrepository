function y = module_output(u,x,N,dt)%u:模型输入序列 x：模型初始状态 N:模型预测输出长度
    x1=zeros(1,N+1);
    x2=zeros(1,N+1);
    y = zeros(1,N);
    x1(1)=x(1);
    x2(1)=x(2);
    for k=1:N
       x1(k+1) = x2(k)*dt;
       x2(k+1) = -(0.3+0.4*x2(k))+(0.4-0.5*x1(k))*x2(k)+sin(0.1*u(k))+u(k);
       y(k) = x1(k+1);
    end
end

