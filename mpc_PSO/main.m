clear;
dt = 1;
T = 100;%仿真时长
P = 6;%预测时域
M = 4;%控制时域
Np = 50;%粒子数量
N  = 100;%迭代次数
X = zeros(Np,M);%粒子位置
V = zeros(Np,M);%粒子速度
c1 = 2;%加速度权重
c2 = 2;
w_max = 0.8;%最大惯性权重
w_min = 0.2;%最小惯性权重
w = w_max;%惯性权重
Pbest = zeros(Np,M);%粒子个体历史最佳位置
J_Pbest = zeros(1,Np);%粒子个体最佳适应度
Gbest = zeros(1,M);%粒子群体历史最佳位置
J_Gbest = 0;%粒子群体最佳适应度
J = zeros(1,Np);%粒子适应度函数
x1 = zeros(1,T);%系统状态
x2 = zeros(1,T);%系统状态
u = zeros(1,T);%控制量
y = zeros(1,T);%输出量
em = zeros(1,T);%预测误差
e  = zeros(1,T);%预测误差
um = zeros(1,P);%优化控制序列
du = zeros(1,M);%优化控制增量序列
ym = zeros(1,T);%单步模型预测输出
ymp = zeros(1,P);%P步模型预测输出
yr = zeros(1,P);
h = [1 1 1 1 1 1];%反馈系数
q = 2;%加权系数Q
r = 1;%加权系数R
for k=1:T
    %*******被控对象*********
    if(k>1)
        x1(k) = x2(k-1);
        x2(k) = -(0.3+0.4*x2(k-1))+(0.4-0.5*x1(k-1))*x2(k-1)+sin(0.1*u(k-1))+u(k-1);
        y(k) = x1(k);
        ym(k) = module_output(u(k-1),[x1(k-1) x2(k-1)],1,dt);%当前模型预测输出
    else
        x1(k) = 0;
        x2(k) = 0;
        y(k) = x1(k);
        ym(k) = 0;
    end
%     yd(k)=sin(k/T*4*pi);
    yd(k)= 1;
    for i=1:P
%         yr(i) = sin((k+i)/T*4*pi);%更新参考轨迹
          yr(i) = 1;
    end
    em(k) = y(k) - ym(k);%当前预测误差
    e(k) = y(k) - yd(k);%当前输出误差
    %********粒子群初始化*******
    for i=1:Np
       X(i,:)=rand(1,M);
       V(i,:)=rand(1,M);
       Pbest(i,:)=X(i,:);
       um(1:M)=X(i,:);
       % M<=i<=P 控制量保持不变
        for j=M+1:P
            um(j:P)=um(M);
        end
        ymp = module_output(um,[x1(k) x2(k)],P,dt);%未来P步模型预测输出
        yp = ymp + h*em(k);%预测输出
        %计算目标函数
        sum1 = 0;
        for j=1:P
            sum1 = sum1+(yp(j)-yr(j))^2;
        end
        for j=1:M
            if j==1
                if k==1
                    du(j) = um(j);
                else
                    du(j) = abs(um(j)-u(k-1));
                end
            else
                du(j) = abs(um(j)- um(j-1));
            end
        end
        sum2 = 0;
        for j=1:M
            sum2 = sum2+du(j)^2;
        end
        J(i) = sum1*q + sum2*r;
        J_Pbest(i) = J(i);%初始化粒子个体历史最佳位置
    end
    %初始化粒子群历史最佳位置
    J_Gbest = J_Pbest(1);
    for i=1:Np
        if(J_Pbest(i) <= J_Gbest)
           Gbest = Pbest(i,:);
           J_Gbest = J(i);
        end
    end
    %********粒子群初始化结束*******
    for t=1:N
        w = w_max-t/N*(w_max-w_min);
        for i=1:Np
            %调整粒子速度和位置
            V(i,:) = w*V(i,:) + c1*rand()*(Pbest(i,:)-X(i,:))+c2*rand()*(Gbest-X(i,:));
            X(i,:) = X(i,:) + V(i,:);
            um(1:M)=X(i,:);
            % M<=i<=P 控制量保持不变
            for j=M+1:P
                um(j)=um(M);
            end
            ymp = module_output(um,[x1(k) x2(k)],P,dt);%未来P步模型预测输出
            yp = ymp;
%             yp = ymp + h*em(k);%反馈校正预测输出
            %计算目标函数
            sum1 = 0;
            for j=1:P
                sum1 = sum1+(yp(j)-yr(j))^2;
            end
            for j=1:M
                if j==1
                    if k==1
                        du(j) = um(j);
                    else
                        du(j) = abs(um(j)-u(k-1));
                    end
                else
                    du(j) = abs(um(j)- um(j-1));
                end
            end
            sum2 = 0;
            for j=1:M
                sum2 = sum2+du(j)^2;
            end
            J(i) = sum1*q + sum2*r;
        end
        %更新粒子历史最佳位置
        for i = 1:Np
            if J(i) < J_Pbest(i)
                Pbest(i,:) =  X(i,:);
                J_Pbest(i) = J(i);
            end
        end
        if J(i) < J_Pbest(i)
            Pbest(i,:) =  X(i,:);
            J_Pbest(i) = J(i);
        end
        %更新粒子群历史最佳位置
        J_Gbest = J_Pbest(1);
        for i=1:Np
            if(J_Pbest(i) <= J_Gbest)
                Gbest = Pbest(i,:);
                J_Gbest = J(i);
            end
        end
    end
    u(k) = Gbest(1); %把最优控制序列中第一个元素作为控制量
end
plot(y,'--');
hold on;
plot(yd,'r');
hold on;
plot(u);
legend('y','yd','u');
figure;
plot(x1,x2);
